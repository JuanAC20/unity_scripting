using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    [System.Serializable]
    [RequireComponent(typeof(CharacterMovement))]
    [RequireComponent(typeof(MouseLook))]
public class PlayerFPSController : MonoBehaviour
{
    // Start is called before the first frame update
    private CharacterMovement characterMovement;
    private MouseLook mouselook;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GameObject.Find("Capsule").gameObject.SetActive(false);  

        characterMovement = GetComponent<characterMovement>();
        mouseLook = GetComponent<MouseLook>();
    }
        private void Update()
    {
        movement();
        rotation();
    }
    private void movement()
    {
        // Movement
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        bool JumpInput = Input.GetButtonDown("Jump");
        bool dashInput = Input.GetButton("Dash");

        characterMovement.moveCharacter(hMovementInput, vMovementInput, JumpInput, dashInput);
    }
    private void rotation()
    {

    float hRotationInput = Input.GetAxis("Mouse X");
    float hRotationInput = Input.GetAxis("Mouse Y");

    mouseLook.handleRotation(hRotationInput, vRotationInput);
    // Update is called once per frame
    }

}

