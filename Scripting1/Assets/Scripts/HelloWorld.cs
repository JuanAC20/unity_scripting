using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelloWorld : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        print("Hola profesor Sergio");
    }

    // Update is called once per frame
    void Update()
    {
        print("Esto corre en tiempo real");
    }
}
